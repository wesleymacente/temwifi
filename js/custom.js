$(function($) {

    var APP = window.APP || {},
        origTitle = document.title,
        state,
        base;

    if (window.location.host == 'localhost')
        base = '';
    else
        base = '';



    APP.init = function() {


            $('input[type=tel]').mask("(99) 9999-9999?9")
                .focusout(function(event) {
                    var target, phone, element;
                    target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                    phone = target.value.replace(/\D/g, '');
                    element = $(target);
                    element.unmask();
                    if (phone.length > 10) {
                        element.mask("(99) 99999-9999");
                    } else {
                        element.mask("(99) 9999-9999?9");
                    }
                });

            $('.cep').mask("99999-999");
            $('#cpf').mask("999.999.999-99");

            $('#c_cep').on('blur', function() {

				if ($.trim($("#c_cep").val()) != "") {

					$("#endereco").html('Aguarde, estamos consultando seu CEP ...');
					$.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=" + $("#c_cep").val(), function() {

						if (resultadoCEP["resultado"]) {
							$("#endereco").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"])).focus();
							$("#bairro").val(unescape(resultadoCEP["bairro"])).focus();
							$("#cidade").val(unescape(resultadoCEP["cidade"])).focus();
							$("#estado").val(unescape(resultadoCEP["uf"])).focus();
						}
					});
				}
			});


            $('#list_empre').change(function() {

                $('#list_lotes').find('option').remove().end();
                var data = "";


            });

        },


        APP.starRatings = function() {

            $(".starrr").starrr();

            $('#velocidade').on('starrr:change', function(e, value) {
                $('#star_velocidade').val(value);
            });
            //serialize star like
            //$('#star_velocidade').val(value);


            $('input:radio[name="aberta"]').change(function(e, value) {

                if ($(this).is(':checked') && $(this).val()==0) {
		            $('.senha_field').removeClass('invisible');
		        } else {
		        	$('.senha_field').addClass('invisible');
		        }

            });

            $('#atendimento').on('starrr:change', function(e, value) {
                $('#star_atendimento').val(value);
            });
            $('#valor').on('starrr:change', function(e, value) {
                $('#star_valor').val(value);
            });
            $('#conforto').on('starrr:change', function(e, value) {
                $('#star_conforto').val(value);
            });
            $('#ruido').on('starrr:change', function(e, value) {
                $('#star_ruido').val(value);
            });
            $('#geral').on('starrr:change', function(e, value) {
                $('#star_geral').val(value);
            });




        },


        $(document).ready(function() {

            APP.init();
            APP.starRatings();

        });


});