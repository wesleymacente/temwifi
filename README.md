
# Projeto Teste: Tem wi-fi?

> Esse é um projeto teste como parte da seleção de Frontenders da
> Plataforma Vibbra!

O aplicativo tem como público alvo profissionais que tem a necessidade de realizar trabalhos fora do escritório. Sejam pessoas que trabalham remotamente, que viajam, profissionais liberais ou que por qualquer outro motivo tenham a necessidade de um lugar para realizar seus trabalhos.

## Estrutura do Projeto

Os arquivos existentes do projeto são basicamente `html` / `css` / `javascript`.
* Arquivos de estilos na pasta `/css`
* Arquivos de javascript, jQuery e Plugins na pasta `/js`
* Webfonts na pasta `/fonts`
* `index.html` - Homepage do WebApp
* `locais.html` - Página listando os resultados encontrados
* `local-detalhe.html` - Página detalhando as informações do local
* `local-avaliar.html` - Área interna para avaliar um local já criado
* `login.html` - Área para fazer login
* `cadastro.html` - Área para criar um cadastro
* `dashboard.html` - Página interna do usuário Logado
* `cadastrar.html` - Página para cadastrar e avaliar um local


## Como rodar a aplicação teste

Para rodar e testar você só precisa abrir o arquivo `index.html` no browser e navegar entre as telas.
Os botões que levam as páginas estão linkados de acordo o fluxo de navegação requisitado.